﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aportaciones.Entidades
{
    public class Movimiento:BaseDTO
    {
        public string IdConjunto { get; set; }
        public string IdPersona { get; set; }
        public float Monto { get; set; }
        public DateTime Pago { get; set; }
        public string IdPeriodo { get; set; }
        public bool EsEntrada { get; set; }
        public string Notas { get; set; }
    }
}
