﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aportaciones.Entidades
{
    public class Persona:BaseDTO
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool EsAdministrador { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string NumeroCasa { get; set; }
        public string IdConjunto { get; set; }
        public override string ToString()
        {
            return $"{Nombre} {Apellidos}";
        }
    }
}
