﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aportaciones.Entidades
{
    public class Periodo:BaseDTO
    {
        public string IdConjunto { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fin { get; set; }
        public string Nombre { get; set; }
        public override string ToString()
        {
            return $"{Nombre} ({Inicio.ToShortDateString()}-{Fin.ToShortDateString()})";
        }
    }
}
