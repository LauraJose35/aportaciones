﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Aportaciones.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CrearCuenta : ContentPage
    {
        public CrearCuenta()
        {
            InitializeComponent();
        }

        private void btnCuentaVecino_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CrearCuentaVecino());
        }

        private void btnCuentaTesorero_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CrearCuentaTesorero());
        }

        private void btnTerminos_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Terminos());
        }
    }
}