﻿using Aportaciones.Entidades;
using Aportaciones.Modelos;
using Aportaciones.Views;
using Aportaciones.Views.Administrador;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Aportaciones
{
    public partial class MainPage : ContentPage
    {
        Repositorio<Persona> repositorio;
        LoginModel model;
        public MainPage()
        {
            InitializeComponent();
            repositorio = new Repositorio<Persona>();
            model = BindingContext as LoginModel;
        }

        private void btnIniciarSesion_Clicked(object sender, EventArgs e)
        {
            try
            {
                Persona usuario = repositorio.Query(p => p.Email == model.Email && p.Password == model.Password).SingleOrDefault();
                if (usuario != null)
                {
                    DisplayAlert("Mis aportaciones", "Bienvenido " + usuario.Nombre, "Ok");
                    if (usuario.EsAdministrador)
                    {
                        Navigation.PushAsync(new MenuAdm(usuario));
                    }
                    else
                    {

                    }
                }
                else
                {
                    DisplayAlert("Error", "Email y/o contraseña incorrecta", "Ok");
                }
            }
            catch (Exception ex)
            {
                DisplayAlert("Error", ex.Message, "Ok");
            }           
        }

        private void btnCrearCuenta_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CrearCuenta());
        }
    }
}
