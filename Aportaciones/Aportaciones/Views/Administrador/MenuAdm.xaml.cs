﻿using Aportaciones.Entidades;
using Aportaciones.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Aportaciones.Views.Administrador
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuAdm : ContentPage
    {
        Persona administrador;
        public MenuAdm(Persona administrador)
        {
            InitializeComponent();
            this.administrador = administrador;
            BindingContext = new MenuAdministradorModel(administrador);
        }

        private void btnPeriodos_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Periodos((BindingContext as MenuAdministradorModel).Conjunto.Id));
        }

        private void btnVecinos_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Vecinos((BindingContext as MenuAdministradorModel).Conjunto.Id));
        }

        private void btnAportaciones_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Aportaciones((BindingContext as MenuAdministradorModel).Conjunto.Id));
        }

        private void btnGastos_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Gastos((BindingContext as MenuAdministradorModel).Conjunto.Id));
        }

        private void btnBalance_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new BalanceGeneral((BindingContext as MenuAdministradorModel).Conjunto.Id));
        }
    }
}