﻿using Aportaciones.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Aportaciones.Views.Administrador
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditarPeriodo : ContentPage
    {
        bool esNuevo;
        Periodo periodo;
        Repositorio<Periodo> repositorio;
        public EditarPeriodo(Periodo periodo)
        {
            InitializeComponent();
            this.periodo = periodo;
            BindingContext = periodo;
            repositorio = new Repositorio<Periodo>();
            if (string.IsNullOrEmpty(periodo.Id)) 
            {
                Title = "Nuevo Periodo";
                esNuevo = true;
            }
            else
            {
                Title = "Editar Periodo";
                esNuevo = false;
            }
        }

        private void btnGuardar_Clicked(object sender, EventArgs e)
        {
            if (repositorio.Create(periodo) != null)
            {
                DisplayAlert("Aportaciones", "Periodo guardado", "OK");
                Navigation.PopAsync();
            }
            else
            {
                DisplayAlert("Error", repositorio.Error, "OK");
            }
        }

        private void btnEliminar_Clicked(object sender, EventArgs e)
        {
            if (repositorio.Delete(periodo))
            {
                DisplayAlert("Aportaciones", "Periodo eliminado", "OK");
                Navigation.PopAsync();
            }
            else
            {
                DisplayAlert("Error", repositorio.Error, "OK");
            }
        }

        private void btnGrafico_Clicked(object sender, EventArgs e)
        {

        }
    }
}