﻿using Aportaciones.Entidades;
using Aportaciones.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Aportaciones.Views.Administrador
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditarGasto : ContentPage
    {
        EditarGastoModel model;
        Repositorio<Movimiento> repositorioMovimientos;
        public EditarGasto(GastoModel gasto)
        {
            InitializeComponent();
            model = new EditarGastoModel(gasto);
            if (string.IsNullOrEmpty(gasto.Movimiento.Id))
            {
                model.GastoItem.Movimiento.Pago = DateTime.Now;
                Title = "Nuevo gasto";
                btnEliminar.IsEnabled = false;
            }
            else
            {
                btnEliminar.IsEnabled = true;
                Title = "Editar gasto";
            }
            BindingContext = model;
            repositorioMovimientos = new Repositorio<Movimiento>();
        }

        private void btnGuardar_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(model.GastoItem.Movimiento.Id))
            {
                model.GastoItem.Movimiento.EsEntrada = false;
                model.GastoItem.Movimiento.IdPeriodo = model.PeriodoSeleccionado.Id;
                //model.GastoItem.Movimiento.IdPersona = model.InquilinoSeleccionado.Id;

                if (repositorioMovimientos.Create(model.GastoItem.Movimiento) != null)
                {
                    DisplayAlert("Aportaciones", "Gasto almacenado correctamente", "OK");
                    Navigation.PopAsync();
                }
                else
                {
                    DisplayAlert("Error", repositorioMovimientos.Error, "OK");
                }
            }
            else
            {
                if (repositorioMovimientos.Update(model.GastoItem.Movimiento) != null)
                {
                    DisplayAlert("Aportaciones", "Gasto almacenado correctamente", "OK");
                    Navigation.PopAsync();
                }
                else
                {
                    DisplayAlert("Error", repositorioMovimientos.Error, "OK");
                }
            }
        }

        private void btnEliminar_Clicked(object sender, EventArgs e)
        {
            if (repositorioMovimientos.Delete(model.GastoItem.Movimiento))
            {
                DisplayAlert("Aportaciones", "Gasto eliminado correctamente", "OK");
                Navigation.PopAsync();
            }
            else
            {
                DisplayAlert("Error", repositorioMovimientos.Error, "OK");
            }
        }
    }
}