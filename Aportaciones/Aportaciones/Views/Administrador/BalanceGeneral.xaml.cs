﻿using Aportaciones.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Aportaciones.Views.Administrador
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BalanceGeneral : ContentPage
    {
        string idConjunto;
        BalanceModel model;
        public BalanceGeneral(string idConjunto)
        {
            try
            {
                InitializeComponent();
                this.idConjunto = idConjunto;
                model = new BalanceModel(idConjunto);
                BindingContext = model;
            }
            catch (Exception ex)
            {
                DisplayAlert("Error", ex.Message, "OK");
            }            
        }

        private void btnBuscar_Clicked(object sender, EventArgs e)
        {
            if (model.Fin < model.Inicio)
            {
                DisplayAlert("Aportaciones","El inicio no puede ser mayor que el fin", "OK");
            }
            else
            {
                grafico.Model = null;
                model.BuscarPorFecha();
                grafico.Model = model.GraficoModel;
                grafico.BackgroundColor = Color.White;
            }
        }
    }
}