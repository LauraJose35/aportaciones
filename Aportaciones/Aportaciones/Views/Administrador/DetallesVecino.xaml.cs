﻿using Aportaciones.Entidades;
using Aportaciones.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Aportaciones.Views.Administrador
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetallesVecino : ContentPage
    {
        VecinoModel vecinoModel;
        public DetallesVecino(Persona vecino)
        {
            InitializeComponent();

           vecinoModel = new VecinoModel(vecino);
           BindingContext = vecinoModel;
           Title = "Aportaciones de "+vecino.Nombre+ " " +vecino.Apellidos;
           grafico.Model = null;
            vecinoModel.GraficaVecino();
           grafico.Model = vecinoModel.GraficoModel;
           grafico.BackgroundColor = Color.White;
        }
    }
}