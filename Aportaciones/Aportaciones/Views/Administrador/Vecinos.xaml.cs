﻿using Aportaciones.Entidades;
using Aportaciones.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Aportaciones.Views.Administrador
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Vecinos : ContentPage
    {
        string idConjunto;
        public Vecinos(string idConjunto)
        {
            InitializeComponent();
            this.idConjunto = idConjunto;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            VecinosModel model = new VecinosModel(idConjunto);
            BindingContext = model;
        }

        private void lstVecinos_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
           
            Navigation.PushAsync(new DetallesVecino(e.SelectedItem as Persona));
        }
    }
}