﻿using Aportaciones.Entidades;
using Aportaciones.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Aportaciones.Views.Administrador
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditarAportacion : ContentPage
    {
        EditarAportacionModel model;
        Repositorio<Movimiento> repositorioMovimientos;
        public EditarAportacion(AportacionModel aportacion)
        {
            InitializeComponent();
            model = new EditarAportacionModel(aportacion);
            if (string.IsNullOrEmpty(aportacion.Movimiento.Id))
            {
                model.AportacionItem.Movimiento.Pago = DateTime.Now;
                Title = "Nueva aportación";
                btnEliminar.IsEnabled = false;
            }
            else
            {
                btnEliminar.IsEnabled = true;
                Title = "Editar aportación";
            }
            BindingContext = model;
            repositorioMovimientos = new Repositorio<Movimiento>();
        }

        private void btnGuardar_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(model.AportacionItem.Movimiento.Id))
            {
                model.AportacionItem.Movimiento.EsEntrada = true;
                model.AportacionItem.Movimiento.IdPeriodo = model.PeriodoSeleccionado.Id;
                model.AportacionItem.Movimiento.IdPersona = model.InquilinoSeleccionado.Id;

                if (repositorioMovimientos.Create(model.AportacionItem.Movimiento) != null)
                {
                    DisplayAlert("Aportaciones", "Aportación almacenada correctamente", "OK");
                    Navigation.PopAsync();
                }
                else 
                {
                    DisplayAlert("Error", repositorioMovimientos.Error,"OK");
                }
            }
            else
            {
                if (repositorioMovimientos.Update(model.AportacionItem.Movimiento) != null)
                {
                    DisplayAlert("Aportaciones", "Aportación almacenada correctamente", "OK");
                    Navigation.PopAsync();
                }
                else
                {
                    DisplayAlert("Error", repositorioMovimientos.Error, "OK");
                }
            }
        }

        private void btnEliminar_Clicked(object sender, EventArgs e)
        {
            if (repositorioMovimientos.Delete(model.AportacionItem.Movimiento))
            {
                DisplayAlert("Aportaciones", "Aportación eliminada correctamente", "OK");
                Navigation.PopAsync();
            }
            else
            {
                DisplayAlert("Error", repositorioMovimientos.Error, "OK");
            }
        }
    }
}