﻿using Aportaciones.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Aportaciones.Views.Administrador
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Periodos : ContentPage
    {
        string idConjunto;
        public Periodos(string idConjunto)
        {
            InitializeComponent();
            this.idConjunto = idConjunto;
           
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            BindingContext = new PeriodosModel(idConjunto);
        }
        private void btnAgregarPeriodo_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new EditarPeriodo(new Entidades.Periodo()
            {

                IdConjunto = idConjunto,
                Inicio = DateTime.Now,
                Fin = DateTime.Now
        }));
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Navigation.PushAsync(new EditarPeriodo((BindingContext as PeriodosModel).PeriodoSeleccionado));
        }
    }
}