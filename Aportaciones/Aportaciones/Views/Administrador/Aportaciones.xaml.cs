﻿using Aportaciones.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Aportaciones.Views.Administrador
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Aportaciones : ContentPage
    {
        string idConjunto;
        public Aportaciones(string idConjunto)
        {
            InitializeComponent();
            this.idConjunto = idConjunto;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            AportacionesModel model = new AportacionesModel(idConjunto);
            BindingContext = model;
        }

        private void lstAportaciones_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Navigation.PushAsync(new EditarAportacion(e.SelectedItem as AportacionModel));
        }

        private void btnAgregar_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new EditarAportacion(new AportacionModel()
            {
                Movimiento= new Entidades.Movimiento()
                {
                    IdConjunto=(BindingContext as AportacionesModel).IdConjunto
                }
            }));
        }
    }
}