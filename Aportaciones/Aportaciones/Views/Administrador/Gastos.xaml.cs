﻿using Aportaciones.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Aportaciones.Views.Administrador
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Gastos : ContentPage
    {
        string idConjunto;
        public Gastos(string idConjunto)
        {
            InitializeComponent();
            this.idConjunto = idConjunto;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            GastosModel model = new GastosModel(idConjunto);
            BindingContext = model;
        }

        private void lstAportaciones_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Navigation.PushAsync(new EditarGasto(e.SelectedItem as GastoModel));
        }

        private void btnAgregar_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new EditarGasto(new GastoModel()
            {
                Movimiento = new Entidades.Movimiento()
                {
                    IdConjunto = (BindingContext as GastosModel).IdConjunto
                }
            }));
        }
    }
}