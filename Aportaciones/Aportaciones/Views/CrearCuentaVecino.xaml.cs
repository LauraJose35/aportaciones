﻿using Aportaciones.Entidades;
using Aportaciones.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Aportaciones.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CrearCuentaVecino : ContentPage
    {
        Repositorio<Persona> personaRepositorio;
        NuevaCuentaVecinoModel model;

        public CrearCuentaVecino()
        {
            InitializeComponent();
            model = BindingContext as NuevaCuentaVecinoModel;
            personaRepositorio = new Repositorio<Persona>();
        }

        private void btnCrearCuenta_Clicked(object sender, EventArgs e)
        {
            if (model.ConjuntoSeleccionado != null)
            {
                if (string.IsNullOrEmpty(model.Password2) || string.IsNullOrEmpty(model.Persona.Apellidos) || string.IsNullOrEmpty(model.Persona.Email) || string.IsNullOrEmpty(model.Persona.Nombre) || string.IsNullOrEmpty(model.Persona.NumeroCasa) || string.IsNullOrEmpty(model.Persona.Password))
                {
                    DisplayAlert("Nueva cuenta", "Datos faltantes", "OK");
                }
                else
                {
                    if (model.Password2 == model.Persona.Password)
                    {
                        model.Persona.IdConjunto = model.ConjuntoSeleccionado.Id;
                        model.Persona.EsAdministrador = false;
                        if (personaRepositorio.Create(model.Persona) != null)
                        {
                            DisplayAlert("Éxito", "Cuenta creada correctamente", "OK");
                        }
                        else {
                            DisplayAlert("Error", personaRepositorio.Error, "OK");
                        }
                    }
                    else
                    {
                        DisplayAlert("Nueva cuenta", "Las contraseñas no son iguales", "OK");
                    }
                }
            }
            else {
                DisplayAlert("Nueva cuenta", "Debes seleccionar un conjunto", "OK");
            }
        }
    }
}