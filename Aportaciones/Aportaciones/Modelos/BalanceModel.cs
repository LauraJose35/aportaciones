﻿using Aportaciones.Entidades;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aportaciones.Modelos
{
    public class BalanceModel
    {
        Repositorio<Persona> repositorioPersona;
        Repositorio<Movimiento> repositorio;
        Repositorio<Periodo> repositorioPeriodo;
        List<Movimiento> TodosLosMovimientos;
        string IdConjunto;
        public List<BalanceItemModel> Movimientos { get; set; }
        public Movimiento MovimientoSeleccionado { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fin { get; set; }
        public float TotalActual { get; set; }
        public DateTime FechaActual { get; set; }
        public PlotModel GraficoModel { get; set; }
        public BalanceModel()
        {

        }
        public BalanceModel(string idConjunto)
        {
            repositorio = new Repositorio<Movimiento>();
            repositorioPersona = new Repositorio<Persona>();
            repositorioPeriodo = new Repositorio<Periodo>();
            Inicio = DateTime.Now;
            Fin = DateTime.Now;
            TodosLosMovimientos = repositorio.Query(m => m.IdConjunto == idConjunto).ToList();
            TotalActual = TodosLosMovimientos.Where(a => a.EsEntrada).Sum(p => p.Monto) - TodosLosMovimientos.Where(a=>!a.EsEntrada).Sum(p=>p.Monto);
            FechaActual = DateTime.Now;
            this.IdConjunto = idConjunto;
        }

        public void BuscarPorFecha()
        {
            List<BalanceItemModel> datos = new List<BalanceItemModel>();
            foreach (var item in TodosLosMovimientos.Where(m=>m.Pago>=Inicio && m.Pago<=Fin))
            {
                datos.Add(new BalanceItemModel() 
                {
                    Concepto=item.EsEntrada?"Casa "+repositorioPersona.SearchById(item.IdPersona).NumeroCasa:item.Notas,
                    EsAportacion=item.EsEntrada,
                    Fecha=item.FechaHora,
                    Monto=item.Monto
                });
            }
            List<PuntoGraficoModel> datosGrafico = new List<PuntoGraficoModel>();
            foreach (var periodo in repositorioPeriodo.Query(p => p.IdConjunto == IdConjunto).OrderBy(f => f.FechaHora))
            {
                var movimientos = TodosLosMovimientos.Where(m => m.IdPeriodo == periodo.Id);
                datosGrafico.Add(new PuntoGraficoModel()
                {
                    Periodo = periodo.Nombre,
                    Monto = movimientos.Where(m => m.EsEntrada).Sum(j => j.Monto) - movimientos.Where(k => !k.EsEntrada).Sum(l => l.Monto)
                });
            }
            GraficoModel = new PlotModel();
            Axis ejesX = new CategoryAxis()
            {
                Position = AxisPosition.Bottom,
                ItemsSource = datosGrafico.Select(d => d.Periodo)
            };
            Axis ejeY = new LinearAxis()
            {
                Position = AxisPosition.Left
            };
            GraficoModel.Axes.Add(ejesX);
            GraficoModel.Axes.Add(ejeY);
            LineSeries linea = new LineSeries();
            linea.ItemsSource = datosGrafico.Select(d => d.Monto);
            GraficoModel.Series.Add(linea);
            linea.Title = "Aportaciones";
            GraficoModel.Title = "Balance general";
        }
    }
}
