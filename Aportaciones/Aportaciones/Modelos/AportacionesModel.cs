﻿using Aportaciones.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aportaciones.Modelos
{
    public class AportacionesModel
    {
        Repositorio<Movimiento> repositorioMovimiento;
        Repositorio<Periodo> repositorioPeriodo;
        Repositorio<Persona> repositorioPersona;
        public string IdConjunto { get; set; }
        public List<AportacionModel> Aportaciones { get; set; }
        public AportacionModel AportacionSeleccionada { get; set; }
        public float Total { get; set; }
        public AportacionesModel()
        {

        }

        public AportacionesModel(string idConjunto)
        {
            IdConjunto = idConjunto;
            repositorioMovimiento = new Repositorio<Movimiento>();
            repositorioPeriodo = new Repositorio<Periodo>();
            repositorioPersona = new Repositorio<Persona>();
            List<Movimiento> movimientos = repositorioMovimiento.Query(m=>m.IdConjunto==idConjunto && m.EsEntrada).ToList();
            List<Periodo> periodos = repositorioPeriodo.Query(p=>p.IdConjunto==idConjunto).ToList();
            List<Persona> personas = repositorioPersona.Query(p=>p.IdConjunto==idConjunto).ToList();
            Aportaciones = new List<AportacionModel>();
            foreach (var movimiento in movimientos)
            {
                Aportaciones.Add(new AportacionModel()
                {
                    Movimiento = movimiento,
                    NombrePeriodo = (periodos.Where(p => p.Id == movimiento.IdPeriodo).SingleOrDefault()).Nombre,
                    NombrePersona = (personas.Where(q=>q.Id==movimiento.IdPersona).SingleOrDefault()).Nombre
                }) ;
            }
            Total = Aportaciones.Sum(a=>a.Movimiento.Monto);
        }
    }
}
