﻿using Aportaciones.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aportaciones.Modelos
{
    public class EditarGastoModel
    {
        Repositorio<Periodo> repositorioPeriodos;
        public GastoModel GastoItem { get; set; }
        public List<Periodo> Periodos { get; set; }
        public Periodo PeriodoSeleccionado { get; set; }
        public EditarGastoModel()
        {

        }
        public EditarGastoModel(GastoModel model)
        {
            GastoItem = model;
            repositorioPeriodos = new Repositorio<Periodo>();
            Periodos = repositorioPeriodos.Query(p => p.IdConjunto == GastoItem.Movimiento.IdConjunto).ToList();
            if (!string.IsNullOrEmpty(model.Movimiento.Id))
            {
                PeriodoSeleccionado = Periodos.SingleOrDefault(p => p.Id == GastoItem.Movimiento.IdPeriodo);
            }

        }
    }
}
