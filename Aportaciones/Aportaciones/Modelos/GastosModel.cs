﻿using Aportaciones.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aportaciones.Modelos
{
    public class GastosModel
    {
        Repositorio<Movimiento> repositorioMovimiento;
        Repositorio<Periodo> repositorioPeriodo;
        public string IdConjunto { get; set; }
        public List<GastoModel> Gastos { get; set; }
        public GastoModel GastoSeleccionado { get; set; }
        public float Total { get; set; }
        public GastosModel()
        {

        }

        public GastosModel(string idConjunto)
        {
            IdConjunto = idConjunto;
            repositorioMovimiento = new Repositorio<Movimiento>();
            repositorioPeriodo = new Repositorio<Periodo>();
            List<Movimiento> movimientos = repositorioMovimiento.Query(m => m.IdConjunto == idConjunto && !m.EsEntrada).ToList();
            List<Periodo> periodos = repositorioPeriodo.Query(p => p.IdConjunto == idConjunto).ToList();
            Gastos = new List<GastoModel>();
            foreach (var movimiento in movimientos)
            {
                Gastos.Add(new GastoModel()
                {
                    Movimiento = movimiento,
                    NombrePeriodo = (periodos.Where(p => p.Id == movimiento.IdPeriodo).SingleOrDefault()).Nombre
                });
            }
            Total = Gastos.Sum(a => a.Movimiento.Monto);
        }
    }
}
