﻿using Aportaciones.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aportaciones.Modelos
{
    public class MenuAdministradorModel
    {
        Repositorio<Movimiento> repositorioMovimientos;
        Repositorio<Conjunto> repositorioConjunto;
        public Persona Administrador { get; set; }
        public float Aportaciones { get; private set; }
        public float Gastos { get; private set; }
        public float Total { get; private set; }
        public Conjunto Conjunto { get; set; }
        public MenuAdministradorModel()
        {

        }
        public MenuAdministradorModel(Persona persona)
        {
            Administrador = persona;
            repositorioMovimientos = new Repositorio<Movimiento>();
            repositorioConjunto = new Repositorio<Conjunto>();
            Conjunto = new Conjunto();
            Conjunto = repositorioConjunto.Query(c=>c.IdRepresentante==Administrador.Id).SingleOrDefault();
            Aportaciones = repositorioMovimientos.Query(m=>m.IdConjunto==Conjunto.Id && m.EsEntrada).Sum(n=>n.Monto);
            Gastos = repositorioMovimientos.Query(m=>m.IdConjunto==Conjunto.Id&& !m.EsEntrada).Sum(n=>n.Monto);
            Total = Aportaciones - Gastos;
        }
    }
}
