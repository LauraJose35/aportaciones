﻿using Aportaciones.Entidades;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aportaciones.Modelos
{
    public class VecinoModel
    {
        Repositorio<Movimiento> repositorioMovimiento;
        public List<Movimiento> MovimientoVecino { get; set; }
        public Persona Vecino { get; set; }
        public List<GraficaVecino> Aportaciones { get; set; }
        public PlotModel GraficoModel { get; set; }
        public VecinoModel()
        {

        }
        public VecinoModel(Persona model)
        {
            repositorioMovimiento = new Repositorio<Movimiento>();
            Aportaciones = new List<GraficaVecino>();
            Vecino = model;
            MovimientoVecino = repositorioMovimiento.Query(m=>m.IdPersona==Vecino.Id).ToList();
            foreach (var item in MovimientoVecino)
            {
                Aportaciones.Add(new GraficaVecino()
                {
                    Fecha=item.FechaHora,
                    Monto=item.Monto,
                    NombreCompleto=Vecino.Nombre+" "+Vecino.Apellidos,
                    Mes= item.FechaHora.ToString("MMMM")
                });
            }
        }

        public void GraficaVecino()
        {
            GraficoModel = new PlotModel();
            GraficoModel = new PlotModel
            {
                Title = "Aportaciones"
            };
            var columnSerie = new ColumnSeries
            {
                StrokeThickness = 2.0
            };

            Axis ejesX = new CategoryAxis()
            {
                Position = AxisPosition.Bottom,
                ItemsSource = Aportaciones.Select(d => d.Mes)
            };
            GraficoModel.Axes.Add(ejesX);
            foreach (var item in Aportaciones)
            {
                columnSerie.Items.Add(new ColumnItem(item.Monto));                
            }
            GraficoModel.Series.Add(columnSerie);
        }
    }
}
