﻿using Aportaciones.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aportaciones.Modelos
{
    public class NuevaCuentaAdminModel
    {
        public Persona Persona { get; set; }
        public Conjunto Conjunto { get; set; }
        public string Password { get; set; }
        public NuevaCuentaAdminModel()
        {
            Persona = new Persona();
            Conjunto = new Conjunto();
        }
    }
}
