﻿using Aportaciones.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aportaciones.Modelos
{
    public class NuevaCuentaVecinoModel
    {
        public Persona Persona { get; set; }
        public string Password2 { get; set; }
        public List<Conjunto> Conjuntos 
        {
            get 
            {
                Repositorio<Conjunto> repositorio = new Repositorio<Conjunto>();
                return repositorio.Read.ToList();
            } 
        }
        public NuevaCuentaVecinoModel()
        {
            Persona = new Persona();
        }
        public Conjunto ConjuntoSeleccionado { get; set; }

    }
}
