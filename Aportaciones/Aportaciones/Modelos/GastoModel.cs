﻿using Aportaciones.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aportaciones.Modelos
{
    public class GastoModel
    {
        public Movimiento Movimiento { get; set; }
        public string NombrePeriodo { get; set; }
        public override string ToString()
        {
            return $"{Movimiento.Notas} ({NombrePeriodo}) {Movimiento.Monto}";
        }
    }
}
