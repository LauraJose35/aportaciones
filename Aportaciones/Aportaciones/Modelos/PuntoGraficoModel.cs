﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aportaciones.Modelos
{
    public class PuntoGraficoModel
    {
        public string Periodo { get; set; }
        public float Monto { get; set; }
    }
}
