﻿using Aportaciones.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aportaciones.Modelos
{
    public class VecinosModel
    {
        Repositorio<Persona> repositorio;
        public List<Persona> Vecinos { get; set; }
        public string IdConjunto { get; set; }
        public Periodo VecinoSeleccionado { get; set; }
        public Persona VecinoItemSeleccionado { get; set; }
        public VecinosModel()
        {

        }
        public VecinosModel(string idConjunto)
        {
            repositorio = new Repositorio<Persona>();
            Vecinos = repositorio.Query(v=>v.IdConjunto==idConjunto).ToList();
            IdConjunto = idConjunto;
        }
    }
}
