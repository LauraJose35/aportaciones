﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aportaciones.Modelos
{
    public class GraficaVecino
    {
        public string NombreCompleto { get; set; }
        public DateTime Fecha { get; set; }
        public string Mes { get; set; }
        public float Monto { get; set; }
    }
}
