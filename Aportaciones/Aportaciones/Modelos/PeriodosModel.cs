﻿using Aportaciones.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aportaciones.Modelos
{
    public class PeriodosModel
    {
        Repositorio<Periodo> repositorioPeriodos;
        public List<Periodo> Periodos { get; set; }
        public Periodo PeriodoSeleccionado { get; set; }
        public PeriodosModel()
        {

        }
        public PeriodosModel(string idConjunto)
        {
            repositorioPeriodos = new Repositorio<Periodo>();
            Periodos = repositorioPeriodos.Query(p => p.IdConjunto == idConjunto).ToList(); ;
        }
    }
}
