﻿using Aportaciones.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aportaciones.Modelos
{
    public class EditarAportacionModel
    {
        Repositorio<Persona> repositorioPersonas;
        Repositorio<Periodo> repositorioPeriodos;
        public AportacionModel AportacionItem { get; set; }
        public List<Persona> Personas { get; set; }
        public List<Periodo> Periodos { get; set; }
        public Persona InquilinoSeleccionado { get; set; }
        public Periodo PeriodoSeleccionado { get; set; }
        public EditarAportacionModel()
        {

        }
        public EditarAportacionModel(AportacionModel model)
        {
            AportacionItem = model;
            repositorioPeriodos = new Repositorio<Periodo>();
            repositorioPersonas = new Repositorio<Persona>();
            Personas = repositorioPersonas.Query(p=>p.IdConjunto==AportacionItem.Movimiento.IdConjunto).ToList();
            Periodos = repositorioPeriodos.Query(p=>p.IdConjunto==AportacionItem.Movimiento.IdConjunto).ToList();
            if (!string.IsNullOrEmpty(model.Movimiento.Id)) 
            {
                InquilinoSeleccionado = Personas.SingleOrDefault(p=>p.Id==AportacionItem.Movimiento.IdPersona);
                PeriodoSeleccionado = Periodos.SingleOrDefault(p=>p.Id==AportacionItem.Movimiento.IdPeriodo);
            }
        
        }
    }
}
