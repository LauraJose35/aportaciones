﻿using Aportaciones.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aportaciones.Modelos
{
    public class AportacionModel
    {
        public Movimiento Movimiento { get; set; }
        public string NombrePersona { get; set; }
        public string NombrePeriodo { get; set; }
        public override string ToString()
        {
            return $"{NombrePersona} ({NombrePeriodo}) {Movimiento.Monto}";
        }
    }
}
